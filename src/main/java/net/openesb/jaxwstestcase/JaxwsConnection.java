package net.openesb.jaxwstestcase;

import com.sun.istack.NotNull;
import com.sun.xml.ws.api.message.Packet;
import com.sun.xml.ws.api.server.PortAddressResolver;
import com.sun.xml.ws.api.server.WSEndpoint;
import com.sun.xml.ws.api.server.WebServiceContextDelegate;
import com.sun.xml.ws.transport.http.HttpAdapter;
import com.sun.xml.ws.transport.http.WSHTTPConnection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.*;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

/**
 * JAX-WS WSHTTPConnection implementation for grizzly transport
 * 
 * @author David BRASSELY
 */
final class JaxwsConnection extends WSHTTPConnection implements WebServiceContextDelegate {

    private final Request request;
    private final Response response;
    private int status;
    private boolean outputWritten;
    private final boolean isSecure;
    private final boolean isAsync;
    final HttpAdapter httpAdapter;

    private Map<String, List<String>> requestHeaders;
    private Map<String, List<String>> responseHeaders;
    
    public JaxwsConnection(final HttpAdapter httpAdapter,
            final Request request, final Response response,
            final boolean isSecure, final boolean isAsync) {
        this.httpAdapter = httpAdapter;
        this.request = request;
        this.response = response;
        this.isSecure = isSecure;
        this.isAsync = isAsync;
    }

    @Override
    @Property({MessageContext.HTTP_REQUEST_HEADERS, Packet.INBOUND_TRANSPORT_HEADERS})
    public @NotNull
    Map<String, List<String>> getRequestHeaders() {
        if (requestHeaders == null) {
            requestHeaders = initializeRequestHeaders();
        }
        
        return requestHeaders;
    }
    
    @Override
    public String getRequestHeader(String headerName) {
        return request.getHeader(headerName);
    }
    
    @Override
    public void setResponseHeaders(Map<String, List<String>> headers) {
        this.responseHeaders = headers;
        if (headers == null) {
            return;

        }
        if (status != 0) {
            response.setStatus(status);
        }
        
        response.reset();   // clear all the headers

        for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
            String name = entry.getKey();
            if (name.equalsIgnoreCase("Content-Type") || name.equalsIgnoreCase("Content-Length")) {
                continue;   // ignore headers that interfere with the operation
            }
            for (String value : entry.getValue()) {
                response.addHeader(name, value);
            }
        }
    }

    @Override
    @Property(MessageContext.HTTP_RESPONSE_HEADERS)
    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }

    @Override
    public void setContentTypeResponseHeader(@NotNull String value) {
        response.setContentType(value);
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    @Property(MessageContext.HTTP_RESPONSE_CODE)
    public int getStatus() {
        return status;
    }

    @NotNull
    @Override
    public InputStream getInput() throws IOException {
        return request.getInputStream();
    }

    @NotNull
    @Override
    public OutputStream getOutput() throws IOException {
        assert !outputWritten;
        outputWritten = true;

        response.setStatus(getStatus());
        return response.getOutputStream();
    }

    @NotNull
    @Override
    public WebServiceContextDelegate getWebServiceContextDelegate() {
        return this;
    }

    @Override
    public Principal getUserPrincipal(Packet request) {
        return this.request.getUserPrincipal();
    }

    @Override
    public boolean isUserInRole(Packet request, String role) {
        return false;
    }

    @NotNull
    @Override
    public String getEPRAddress(Packet p, WSEndpoint endpoint) {
        PortAddressResolver resolver = httpAdapter.owner.createPortAddressResolver(getBaseAddress(), endpoint.getClass());
        String address = resolver.getAddressFor(endpoint.getServiceName(), endpoint.getPortName().getLocalPart());
        if (address == null) {
            throw new WebServiceException("WsservletMessages.SERVLET_NO_ADDRESS_AVAILABLE(" + endpoint.getPortName() + ")");
        }
        return address;
    }

    @Override
    public String getWSDLAddress(@NotNull Packet request, @NotNull WSEndpoint endpoint) {
        String eprAddress = getEPRAddress(request, endpoint);
        return eprAddress + "?wsdl";
    }

    @NotNull
    public String getBaseAddress() {
        return getBaseAddress(request);
    }
    
    static @NotNull
    String getBaseAddress(Request request) {
        StringBuilder buf = new StringBuilder();
        buf.append(request.getScheme());
        buf.append("://");
        buf.append(request.getServerName());
        buf.append(':');
        buf.append(request.getServerPort());
        buf.append(request.getContextPath());
        
        final String httpHandlerPath = request.getHttpHandlerPath();
        if (httpHandlerPath != null) {
            buf.append(httpHandlerPath);
        }

        return buf.toString();
    }

    @Override
    public boolean isSecure() {
        return this.isSecure;
    }

    @Override
    @Property(MessageContext.HTTP_REQUEST_METHOD)
    public @NotNull
    String getRequestMethod() {
        return request.getMethod().getMethodString();
    }

    @Override
    @Property(MessageContext.QUERY_STRING)
    public String getQueryString() {
        return request.getQueryString();
    }

    @Override
    @Property(MessageContext.PATH_INFO)
    public String getPathInfo() {
        return request.getPathInfo();
    }

    /**
     * Override the close to make sure the Grizzly ARP processing completes
     * Delegate further processing to parent class
     */
    @Override
    public void close() {
        try {
            super.close();
        } finally {
            if (isAsync) {
                response.resume();
            }
        }
    }

    @Override
    protected PropertyMap getPropertyMap() {
        return model;
    }
    private static final PropertyMap model;

    static {
        model = parse(JaxwsConnection.class);
    }

    private Map<String, List<String>> initializeRequestHeaders() {
        final Map<String, List<String>> headers = new HashMap<String, List<String>>();
        for (String name : request.getHeaderNames()) {
            final List<String> values = new ArrayList<String>(2);
            for (String value : request.getHeaders(name)) {
                values.add(value);
            }
            
            headers.put(name, values);
        }
        
        return headers;
    }

    @Override
    public void setResponseHeader(String headerName, List<String> values) {
        responseHeaders.put(headerName, values);
    }

    @Override
    public Set<String> getRequestHeaderNames() {
        return requestHeaders.keySet();
    }

    @Override
    public List<String> getRequestHeaderValues(String headerName) {
        return requestHeaders.get(headerName);
    }

    @Override
    public String getRequestURI() {
        return request.getRequestURI();
    }

    @Override
    public String getRequestScheme() {
        return request.getScheme();
    }

    @Override
    public String getServerName() {
        return request.getServerName();
    }

    @Override
    public int getServerPort() {
        return request.getServerPort();
    }
}

