package net.openesb.jaxwstestcase;

import java.io.IOException;
import javax.xml.namespace.QName;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;

/**
 *
 * @author David BRASSELY
 */
public class GrizzlyWebServer {

    public static void main(String[] args) throws IOException {
    //    System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
    //    System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        
        HttpServer httpServer = new HttpServer();
        NetworkListener networkListener = new NetworkListener("jaxws-listener", "0.0.0.0", 9876);

        HttpHandler httpHandler = new JaxwsHandler(
                GrizzlyWebServer.class.getResource("/service.wsdl"), 
                new QName("MTOMTransferCA", "MTOMTransferCAService1"),
                new QName("MTOMTransferCA", "casaPort1"), 
                false /* Grizzly async */);
        
        httpServer.getServerConfiguration().addHttpHandler(httpHandler, "/ws");
        
        httpServer.addListener(networkListener);

        httpServer.start();
        
        System.out.println("Press any key to stop the server...");
        System.in.read();
    }
}
