package net.openesb.jaxwstestcase.util;

import java.util.LinkedList;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;

public class TransformerPool {
    private static final TransformerFactory cTransformerFact =
            TransformerFactory.newInstance();
    
    private final LinkedList<Transformer> mTransformers;

    /** Creates a new instance of TransformerPool */
    public TransformerPool() {
        mTransformers = new LinkedList<Transformer>();
    }
    
    public TransformerPool(int size) throws TransformerConfigurationException {
        this();
        for (int i = 0; i < size; ++i) {
            mTransformers.addFirst(cTransformerFact.newTransformer());
        }
    }
    
    public Transformer retrieve() throws TransformerConfigurationException {
        Transformer transformer = null;
        
        synchronized(this) {
            if (!mTransformers.isEmpty()) {
                transformer = mTransformers.removeFirst();
            } else {
                transformer = cTransformerFact.newTransformer();
            }
        }
        return transformer;
    }
    
    public boolean relinquish(Transformer transformer) {
        boolean success = false;
        if (transformer != null) {
            synchronized (this) {
                if (!mTransformers.contains(transformer)) {
                    mTransformers.addFirst(transformer);
                    success = true;
                }
            }
        }
        return success;
    }
}

