package net.openesb.jaxwstestcase;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import net.openesb.jaxwstestcase.util.DebugLog;

/**
 *
 * @author David BRASSELY
 */
@javax.xml.ws.RespectBinding
@ServiceMode(Service.Mode.MESSAGE)
@WebServiceProvider()
public class CustomProvider implements Provider<SOAPMessage> {
    private static final Logger LOGGER = Logger.getLogger("CustomProvider");
    
    public SOAPMessage invoke(SOAPMessage request) {
        AttachmentPart attch = (AttachmentPart) request.getAttachments().next();
        
        try {
            DebugLog.debugLog(LOGGER, Level.WARNING, "toto", request.getSOAPBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return request;
    }
}
