package net.openesb.jaxwstestcase;

import com.sun.xml.ws.api.BindingID;
import com.sun.xml.ws.api.WSBinding;
import com.sun.xml.ws.api.server.Container;
import com.sun.xml.ws.api.server.InstanceResolver;
import com.sun.xml.ws.api.server.Invoker;
import com.sun.xml.ws.api.server.SDDocumentSource;
import com.sun.xml.ws.api.server.WSEndpoint;
import com.sun.xml.ws.transport.http.HttpAdapter;
import com.sun.xml.ws.transport.http.WSHTTPConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.soap.SOAPBinding;
import org.glassfish.grizzly.EmptyCompletionHandler;
import org.glassfish.grizzly.http.Method;
import org.glassfish.grizzly.http.server.HttpHandler;

import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.xml.sax.EntityResolver;

/**
 *
 * @author David BRASSELY
 */
public class JaxwsHandler extends HttpHandler {

    private static final Logger LOGGER = Logger.getLogger("JaxwsHandler");
    private final boolean isAsync;
    private final QName service, port;
    private final URL wsdl;
    private WSEndpoint endpoint;
    private HttpAdapter httpAdapter;

    public JaxwsHandler(URL wsdl, QName service, QName port, boolean isAsync) {
        this.wsdl = wsdl;
        this.service = service;
        this.port = port;
        this.isAsync = isAsync;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        Class serviceEndpointClass = CustomProvider.class;
        Invoker invoker = InstanceResolver.createDefault(serviceEndpointClass).createInvoker();
        
        Container container = null; /* no container */

        BindingID bindingID = BindingID.parse(
                SOAPBinding.SOAP11HTTP_MTOM_BINDING);
        WSBinding binding = bindingID.createBinding();
        
        attachDefaultHandlers(binding);
        
        try {
            SDDocumentSource primaryWsdl = SDDocumentSource.create(wsdl);

            this.endpoint = WSEndpoint.create(
                    serviceEndpointClass,
                    false,
                    invoker,
                    service,
                    port,
                    container,
                    binding,
                    primaryWsdl,
                    null, //TODO
                    (EntityResolver) null,
                    !isAsync);

            this.httpAdapter = HttpAdapter.createAlone(endpoint);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    private void attachDefaultHandlers(WSBinding binding) {
        List<Handler> handlers = new ArrayList<Handler>();
        handlers.add(new MTOMHandler());
        binding.setHandlerChain(handlers);
    }

    /**
     * Main entry point of the {@link HttpHandler} to service a request
     *
     * @param req incoming HTTP request
     * @param res HTTP response to prepare
     */
    @Override
    public void service(Request req, Response res) throws Exception {
        LOGGER.log(Level.FINE, "Received a request. The request thread {0} .", Thread.currentThread());
        // TODO: synchornous execution for ?wsdl, non AsyncProvider requests
        final WSHTTPConnection connection = new JaxwsConnection(httpAdapter,
                req, res, req.isSecure(), isAsync);


        if (Method.GET.equals(req.getMethod())) {
            // metadata query. let the interceptor run
            if (isMetadataQuery(connection.getQueryString())) {
                // Sends published WSDL and schema documents as the default action.
                httpAdapter.publishWSDL(connection);
                return;
            }
        }

        if (isAsync) {
            res.suspend(-1, TimeUnit.MILLISECONDS,
                    new EmptyCompletionHandler<Response>() {
                        @Override
                        public void failed(Throwable throwable) {
                            if (LOGGER.isLoggable(Level.FINE)) {
                                LOGGER.log(Level.FINE, "Forcing connection close on a connection: " + connection, throwable);
                            }

                            connection.close();
                        }
                    });
            httpAdapter.invokeAsync(connection);
        } else {
            httpAdapter.handle(connection);
        }

        LOGGER.log(Level.FINE, "Getting out of service(). Done with the request thread {0} .", Thread.currentThread());
    }

    private boolean isMetadataQuery(String query) {
        // we intentionally return true even if documents don't exist,
        // so that they get 404.
        return query != null && (query.equals("WSDL") || query.startsWith("wsdl") || query.startsWith("xsd="));
    }
}
