/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.openesb.jaxwstestcase;

import java.io.ByteArrayInputStream;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import net.openesb.jaxwstestcase.util.DebugLog;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author David BRASSELY
 */
public class MTOMHandler implements SOAPHandler<SOAPMessageContext> {

    private static final Logger LOGGER = Logger.getLogger("MTOMHandler");

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        if (Boolean.FALSE.equals(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY))) {

            try {
                SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
                resolveBase64Content(context.getMessage(), envelope.getBody());
                
                context.getMessage().saveChanges();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void resolveBase64Content(SOAPMessage soapMessage, Node node) {
        NodeList nl = node.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);

            if (n instanceof Element) {
                Element elt = (Element) n;
                
                if (isBase64(elt.getTextContent())) {
                    addAttachmentPart(soapMessage, elt);
                } else {
                    resolveBase64Content(soapMessage, n);
                }
            } else {
                resolveBase64Content(soapMessage, n);
            }
        }
    }

    private void addAttachmentPart(SOAPMessage soapMessage, Element elt) {
        try {
            // New attachment reference
            String uuid = UUID.randomUUID().toString();
            String contentLocation = uuid + "@mtom.jaxws.openesb.net";
            
            // Create a new attachment part.
            AttachmentPart attachment = soapMessage.createAttachmentPart();
            attachment.setBase64Content(new ByteArrayInputStream(
                    elt.getTextContent().getBytes()), null);

            // Try to retrieve the old attachment by querying using the size of the stream.
            AttachmentPart oldAttachment = resolveAttachment(soapMessage, attachment.getSize());
            
            attachment.setContentLocation(contentLocation);
            attachment.setContentId(uuid);
            attachment.setContentType(oldAttachment.getContentType());
            attachment.setDataHandler(oldAttachment.getDataHandler());

            // Remove old attachment
            MimeHeaders headers = new MimeHeaders();
            for (Iterator mimeHeaders = oldAttachment.getAllMimeHeaders(); mimeHeaders.hasNext();) {
                MimeHeader mimeHeader = (MimeHeader) mimeHeaders.next();
                headers.addHeader(mimeHeader.getName(), mimeHeader.getValue());
            }
            soapMessage.removeAttachments(headers);

            // Copy mime headers from old attachment
            for (Iterator mimeHeaders = oldAttachment.getAllMimeHeaders(); mimeHeaders.hasNext();) {
                MimeHeader mimeHeader = (MimeHeader) mimeHeaders.next();
                attachment.addMimeHeader(mimeHeader.getName(), mimeHeader.getValue());
            }

            soapMessage.addAttachmentPart(attachment);

            // Remove inline content
            elt.setTextContent("");

            // And replace it with XOP:include
            SOAPElement cc = soapMessage.getSOAPBody().addChildElement(new QName("http://www.w3.org/2004/08/xop/include", "Include", "xop"));
            cc.addAttribute(new QName("href"), "cid:" + attachment.getContentLocation());
            elt.appendChild(cc);
            
        } catch (SOAPException ex) {
            ex.printStackTrace();
        }
    }

    private AttachmentPart resolveAttachment(SOAPMessage soapMessage, long size) throws SOAPException {
        for (Iterator attachments = soapMessage.getAttachments(); attachments.hasNext();) {
            AttachmentPart attachment = (AttachmentPart) attachments.next();

            if (attachment.getSize() == size) {
                return attachment;
            }
        }

        return null;
    }

    public static boolean isBase64(String base64String) {
        if (base64String.replace(" ", "").length() % 4 != 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }
}
